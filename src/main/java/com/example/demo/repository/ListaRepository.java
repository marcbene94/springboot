package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Lista;

@Repository
public interface ListaRepository extends JpaRepository<Lista, Integer>{

	@Query(value = "SELECT id FROM task.lista order by id desc limit 1", nativeQuery=true)
	int getLastId();

}
