package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Lista;
import com.example.demo.entity.Tarjeta;
import com.example.demo.service.ListaService;
import com.example.demo.service.TarjetaService;

@RestController
@RequestMapping("/api")
public class MainController {
	
	@Autowired
	private TarjetaService tarjetaService;
	
	@Autowired
	private ListaService listaService;
	
	private List<Object> listas_y_tarjetas = new ArrayList<Object>();
	
	@GetMapping("/getAllData")
	public List<Object> readAll() {
		List<Lista> listas = (List<Lista>) listaService.findAll();
		
		List<Tarjeta> tarjetas = (List<Tarjeta>) tarjetaService.findAll();
		
		this.listas_y_tarjetas.add(listas);
		this.listas_y_tarjetas.add(tarjetas);
		
		return this.listas_y_tarjetas;
	}

}
