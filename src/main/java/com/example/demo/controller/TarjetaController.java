package com.example.demo.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.TarjetaService;
import com.example.demo.entity.Tarjeta;

@RestController
@RequestMapping("/api")
public class TarjetaController {

	@Autowired
	private TarjetaService tarjetaService;
	
	@GetMapping("/tarjetas")
	public List<Tarjeta> readAll() {
		List<Tarjeta> tarjetas = (List<Tarjeta>) tarjetaService.findAll();
		
		return tarjetas;
	}
	
}