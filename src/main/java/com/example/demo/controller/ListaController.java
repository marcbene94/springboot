package com.example.demo.controller;


import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ListaService;
import com.example.demo.entity.Lista;

@RestController
@RequestMapping("/api")
public class ListaController {

	@Autowired
	private ListaService listaService;
	
	@GetMapping("/listas")
	public List<Lista> readAll() {
		List<Lista> listas = (List<Lista>) listaService.findAll();
		
		return listas;
	}
	
	@GetMapping("/getLastListId")
	public JSONObject getLastListId() {
		int id = listaService.getLastId();
		
		JSONObject response = new JSONObject();
		response.put("data", id);
		
		return response;
	}
	
}
