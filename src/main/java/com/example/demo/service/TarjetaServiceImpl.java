package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Tarjeta;
import com.example.demo.repository.TarjetaRepository;

@Service
public class TarjetaServiceImpl implements TarjetaService {
	
	@Autowired
	private TarjetaRepository tarjetaRepository;

	@Override
	@Transactional(readOnly=true)
	public List<Tarjeta> findAll() {
		// TODO Auto-generated method stub
		return tarjetaRepository.findAll();
	}

}
