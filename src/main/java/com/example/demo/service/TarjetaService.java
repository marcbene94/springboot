package com.example.demo.service;

import com.example.demo.entity.Tarjeta;

public interface TarjetaService {
	
	public Iterable<Tarjeta> findAll();

}
