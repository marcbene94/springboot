package com.example.demo.service;

import com.example.demo.entity.Lista;

public interface ListaService {
	
	public Iterable<Lista> findAll();
	
	public int getLastId();

}
