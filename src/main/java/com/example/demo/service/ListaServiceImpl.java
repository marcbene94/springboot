package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Lista;
import com.example.demo.repository.ListaRepository;

@Service
public class ListaServiceImpl implements ListaService {

	@Autowired
	private ListaRepository listaRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Lista> findAll() {
		// TODO Auto-generated method stub
		return listaRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public int getLastId() {
		return listaRepository.getLastId();
	}

}