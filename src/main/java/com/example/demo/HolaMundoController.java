package com.example.demo;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class HolaMundoController {
	
	@GetMapping("/inicio")
    public JSONObject inicio() {
        String url = "https://pokeapi.co/api/v2/pokemon/ditto";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        JSONObject respuesta = getJsonFromString(result);
        
        return respuesta;
        
    }
	
	@GetMapping("/obtenerDatosPokemon/{urlParameter}")
    public JSONObject obtenerDatosPokemon(@PathVariable("urlParameter") String pokemon_indicado) {
		String url = "https://pokeapi.co/api/v2/pokemon/" + pokemon_indicado;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        JSONObject respuesta = getJsonFromString(result);
        
        return respuesta;
        
    }
	
	public JSONObject getJsonFromString(String resultado_de_api) {
		JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
		try {
			json = (JSONObject) parser.parse(resultado_de_api);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json;
	}
}